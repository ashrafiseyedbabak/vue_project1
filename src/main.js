import Vue from 'vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import { BCard, BCardBody, BCardFooter, BCardHeader, BCardImg, BTable } from 'bootstrap-vue'
import App from './App.vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'


Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.config.productionTip = false

Vue.component('b-card', BCard)
Vue.component('b-card-body', BCardBody)
Vue.component('b-card-footer', BCardFooter)
Vue.component('b-card-header', BCardHeader)
Vue.component('b-card-img', BCardImg)
Vue.component('b-table', BTable)

export default {
  components: {
    BCard,
    BCardBody,
    BCardFooter,
    BCardHeader,
    BCardImg,
    BTable
  }
  // ...
}

new Vue({
  render: h => h(App),
}).$mount('#app')
